<?php

namespace Webberig\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Locale\Exception\NotImplementedException;

/**
 * ContentPages
 *
 * @ORM\Table(name="content_pages")
 * @ORM\Entity
 */
class ContentPage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Language", inversedBy="pages")
     * @ORM\JoinColumn(name="language", referencedColumnName="id", nullable=false)
     */
    private $language;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="ContentCategory", inversedBy="pages")
     * @ORM\JoinColumn(name="category", referencedColumnName="id", nullable=false)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="intro", type="text", nullable=true)
     */
    private $intro;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="date", nullable=true)
     */
    private $startdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stopDate", type="date", nullable=true)
     */
    private $stopdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="itemDate", type="date", nullable=true)
     */
    private $itemdate;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle1", type="string", length=255, nullable=true)
     */
    private $subtitle1;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle2", type="string", length=255, nullable=true)
     */
    private $subtitle2;

    /**
     * @var integer
     *
     * @ORM\Column(name="parentID", type="integer", nullable=true)
     */
    private $parentid;

    /**
     * @var string
     *
     * @ORM\Column(name="modifiedBy", type="string", length=45, nullable=true)
     */
    private $modifiedby;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modifiedDate", type="date", nullable=true)
     */
    private $modifieddate;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set language
     *
     * @param integer $language
     * @return ContentPages
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    
        return $this;
    }

    /**
     * Get language
     *
     * @return integer 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ContentPages
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ContentPages
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set intro
     *
     * @param string $intro
     * @return ContentPages
     */
    public function setIntro($intro)
    {
        $this->intro = $intro;
    
        return $this;
    }

    /**
     * Get intro
     *
     * @return string 
     */
    public function getIntro()
    {
        return $this->intro;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return ContentPages
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return ContentPages
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set startdate
     *
     * @param \DateTime $startdate
     * @return ContentPages
     */
    public function setStartdate($startdate)
    {
        $date = new \DateTime();
        $date = $date->createFromFormat("d/m/Y", $startdate);
        if ($date === false)
            throw new \Exception("Unable to parse startdate");
        $this->startdate = $date;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return \DateTime 
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set stopdate
     *
     * @param \DateTime $stopdate
     * @return ContentPages
     */
    public function setStopdate($stopdate)
    {
        $date = new \DateTime();
        $date = $date->createFromFormat("d/m/Y", $stopdate);
        if ($date === false)
            throw new \Exception("Unable to parse stopdate");
        $this->stopdate = $date;

        return $this;
    }

    /**
     * Get stopdate
     *
     * @return \DateTime 
     */
    public function getStopdate()
    {
        return $this->stopdate;
    }

    /**
     * Set itemdate
     *
     * @param \DateTime $itemdate
     * @return ContentPages
     */
    public function setItemdate($itemdate)
    {
        $date = new \DateTime();
        $date = $date->createFromFormat("d/m/Y", $itemdate);
        if ($date === false)
            throw new \Exception("Unable to parse itemdate");
        $this->itemdate = $date;
    
        return $this;
    }

    /**
     * Get itemdate
     *
     * @return \DateTime 
     */
    public function getItemdate()
    {
        return $this->itemdate;
    }

    /**
     * Set subtitle1
     *
     * @param string $subtitle1
     * @return ContentPages
     */
    public function setSubtitle1($subtitle1)
    {
        $this->subtitle1 = $subtitle1;
    
        return $this;
    }

    /**
     * Get subtitle1
     *
     * @return string 
     */
    public function getSubtitle1()
    {
        return $this->subtitle1;
    }

    /**
     * Set subtitle2
     *
     * @param string $subtitle2
     * @return ContentPages
     */
    public function setSubtitle2($subtitle2)
    {
        $this->subtitle2 = $subtitle2;
    
        return $this;
    }

    /**
     * Get subtitle2
     *
     * @return string 
     */
    public function getSubtitle2()
    {
        return $this->subtitle2;
    }

    /**
     * Set parentid
     *
     * @param integer $parentid
     * @return ContentPages
     */
    public function setParentid($parentid)
    {
        $this->parentid = $parentid;
    
        return $this;
    }

    /**
     * Get parentid
     *
     * @return integer 
     */
    public function getParentid()
    {
        return $this->parentid;
    }

    /**
     * Set modifiedby
     *
     * @param string $modifiedby
     * @return ContentPages
     */
    public function setModifiedby($modifiedby)
    {
        $this->modifiedby = $modifiedby;
    
        return $this;
    }

    /**
     * Get modifiedby
     *
     * @return string 
     */
    public function getModifiedby()
    {
        return $this->modifiedby;
    }

    /**
     * Set modifieddate
     *
     * @param \DateTime $modifieddate
     * @return ContentPages
     */
    public function setModifieddate($modifieddate)
    {
        $this->modifieddate = $modifieddate;
    
        return $this;
    }

    /**
     * Get modifieddate
     *
     * @return \DateTime 
     */
    public function getModifieddate()
    {
        return $this->modifieddate;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return ContentPage
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set category
     *
     * @param \Webberig\CMSBundle\Entity\ContentCategory $category
     * @return ContentPage
     */
    public function setCategory(\Webberig\CMSBundle\Entity\ContentCategory $category)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Webberig\CMSBundle\Entity\ContentCategory 
     */
    public function getCategory()
    {
        return $this->category;
    }

    public function getFullUrl()
    {
        if (!$this->getCategory()->getUseurl())
            throw new \Exception("De categorie van deze pagina bevat geen URL's");
        $url = $this->getLanguage()->getAbbreviation() . "/";

        $category_slug = $this->getCategory()->getUrlprefix();
        if (!empty($category_slug))
            $url .= $category_slug . "/";

        $url .= $this->getUrl();
        return $url;
    }

    public function isPublic() {
        $now = new \DateTime();
        if ($this->getActive() == false) {
            return false;
        }
        if ($this->getCategory()->getUsedate()) {
            if ($this->getStartdate() > $now) {
                return false;
            }
            if ($this->getStopdate() <= $now) {
                return false;
            }
        }
        return true;
    }
}
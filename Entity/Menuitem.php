<?php

namespace Webberig\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Menuitem
 *
 * @ORM\Table(name="menuitems")
 * @ORM\Entity
 */
class Menuitem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Menugroup", inversedBy="menu")
     * @ORM\JoinColumn(name="menugroup", referencedColumnName="id", nullable=false)
     */

    private $menugroup;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(name="language", referencedColumnName="id", nullable=false)
     */
    private $language;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Menuitem", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id", nullable=true)
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="sequence", type="integer", nullable=true)
     */
    private $sequence;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=45, nullable=true)
     */
    private $url;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="ContentPage")
     * @ORM\JoinColumn(name="page", referencedColumnName="id", nullable=true)
     */
    private $page;


    /**
     * @ORM\OneToMany(targetEntity="Menuitem", mappedBy="parent")
     * @ORM\OrderBy({"sequence" = "ASC"})
     */
    protected $submenus;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group
     *
     * @param integer $group
     * @return Menuitems
     */
    public function setGroup($group)
    {
        $this->group = $group;
    
        return $this;
    }

    /**
     * Get group
     *
     * @return integer 
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set language
     *
     * @param integer $language
     * @return Menuitems
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    
        return $this;
    }

    /**
     * Get language
     *
     * @return integer 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set parent
     *
     * @param integer $parent
     * @return Menuitems
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return integer 
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Menuitems
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return Menuitems
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Menuitems
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set page
     *
     * @param integer $page
     * @return Menuitems
     */
    public function setPage($page)
    {
        $this->page = $page;
    
        return $this;
    }

    /**
     * Get page
     *
     * @return integer 
     */
    public function getPage()
    {
        return $this->page;
    }

    public function getPageId()
    {
        if ($this->page == null)
            return null;
        return $this->getPage()->getId();
    }

    /**
     * Set menugroup
     *
     * @param \Webberig\CMSBundle\Entity\Menugroup $menugroup
     * @return Menuitem
     */
    public function setMenugroup(\Webberig\CMSBundle\Entity\Menugroup $menugroup)
    {
        $this->menugroup = $menugroup;
    
        return $this;
    }

    /**
     * Get menugroup
     *
     * @return \Webberig\CMSBundle\Entity\Menugroup 
     */
    public function getMenugroup()
    {
        return $this->menugroup;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->submenus = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add submenus
     *
     * @param \Webberig\CMSBundle\Entity\Menuitem $submenus
     * @return Menuitem
     */
    public function addSubmenu(\Webberig\CMSBundle\Entity\Menuitem $submenus)
    {
        $this->submenus[] = $submenus;
    
        return $this;
    }

    /**
     * Remove submenus
     *
     * @param \Webberig\CMSBundle\Entity\Menuitem $submenus
     */
    public function removeSubmenu(\Webberig\CMSBundle\Entity\Menuitem $submenus)
    {
        $this->submenus->removeElement($submenus);
    }

    /**
     * Get submenus
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubmenus()
    {
        return $this->submenus;
    }
}
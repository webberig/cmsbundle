<?php

namespace Webberig\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContentCategories
 *
 * @ORM\Table(name="content_categories")
 * @ORM\Entity
 */
class ContentCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=15, nullable=false)
     */
    private $slug;

    /**
     * @var integer
     *
     * @ORM\Column(name="sequence", type="integer", nullable=true)
     */
    private $sequence;

    /**
     * @var boolean
     *
     * @ORM\Column(name="useMETA", type="boolean", nullable=false)
     */
    private $usemeta;

    /**
     * @var boolean
     *
     * @ORM\Column(name="useIntro", type="boolean", nullable=false)
     */
    private $useintro;

    /**
     * @var boolean
     *
     * @ORM\Column(name="useContent", type="boolean", nullable=false)
     */
    private $usecontent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="useURL", type="boolean", nullable=false)
     */
    private $useurl;

    /**
     * @var string
     *
     * @ORM\Column(name="urlPrefix", type="string", length=45, nullable=false)
     */
    private $urlprefix;

    /**
     * @var boolean
     *
     * @ORM\Column(name="useDateRange", type="boolean", nullable=false)
     */
    private $usedaterange;

    /**
     * @var boolean
     *
     * @ORM\Column(name="useDate", type="boolean", nullable=true)
     */
    private $usedate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="useSubtitle1", type="boolean", nullable=true)
     */
    private $usesubtitle1;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle1_label", type="string", length=45, nullable=true)
     */
    private $subtitle1Label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="useSubtitle2", type="boolean", nullable=true)
     */
    private $usesubtitle2;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle2_label", type="string", length=45, nullable=true)
     */
    private $subtitle2Label;

    /**
     * @var boolean
     *
     * @ORM\Column(name="useParent", type="boolean", nullable=true)
     */
    private $useparent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="useGallery", type="boolean", nullable=true)
     */
    private $useGallery;

    /**
     * @var integer
     *
     * @ORM\Column(name="parentCategory", type="integer", nullable=true)
     */
    private $parentcategory;


    /*******************************************************************************************************************
    *** Relationships
    *******************************************************************************************************************/

    /**
     * @ORM\OneToMany(targetEntity="ContentPage", mappedBy="category")
     * @ORM\OrderBy({"title" = "ASC"})
     */
    protected $pages;

    /**
     * @ORM\OneToMany(targetEntity="ContentCategoryLink", mappedBy="category")
     */
    protected $categoryLinks;


    public function __construct()
    {
        $this->pages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ContentCategories
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sequence
     *
     * @param integer $sequence
     * @return ContentCategories
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    
        return $this;
    }

    /**
     * Get sequence
     *
     * @return integer 
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set usemeta
     *
     * @param boolean $usemeta
     * @return ContentCategories
     */
    public function setUsemeta($usemeta)
    {
        $this->usemeta = $usemeta;
    
        return $this;
    }

    /**
     * Get usemeta
     *
     * @return boolean 
     */
    public function getUsemeta()
    {
        return $this->usemeta;
    }

    /**
     * Set useintro
     *
     * @param boolean $useintro
     * @return ContentCategories
     */
    public function setUseintro($useintro)
    {
        $this->useintro = $useintro;
    
        return $this;
    }

    /**
     * Get useintro
     *
     * @return boolean 
     */
    public function getUseintro()
    {
        return $this->useintro;
    }

    /**
     * Set usecontent
     *
     * @param boolean $usecontent
     * @return ContentCategories
     */
    public function setUsecontent($usecontent)
    {
        $this->usecontent = $usecontent;
    
        return $this;
    }

    /**
     * Get usecontent
     *
     * @return boolean 
     */
    public function getUsecontent()
    {
        return $this->usecontent;
    }

    /**
     * Set useurl
     *
     * @param boolean $useurl
     * @return ContentCategories
     */
    public function setUseurl($useurl)
    {
        $this->useurl = $useurl;
    
        return $this;
    }

    /**
     * Get useurl
     *
     * @return boolean 
     */
    public function getUseurl()
    {
        return $this->useurl;
    }

    /**
     * Set urlprefix
     *
     * @param string $urlprefix
     * @return ContentCategories
     */
    public function setUrlprefix($urlprefix)
    {
        $this->urlprefix = $urlprefix;
    
        return $this;
    }

    /**
     * Get urlprefix
     *
     * @return string 
     */
    public function getUrlprefix()
    {
        return $this->urlprefix;
    }

    /**
     * Set usedaterange
     *
     * @param boolean $usedaterange
     * @return ContentCategories
     */
    public function setUsedaterange($usedaterange)
    {
        $this->usedaterange = $usedaterange;
    
        return $this;
    }

    /**
     * Get usedaterange
     *
     * @return boolean 
     */
    public function getUsedaterange()
    {
        return $this->usedaterange;
    }

    /**
     * Set usedate
     *
     * @param boolean $usedate
     * @return ContentCategories
     */
    public function setUsedate($usedate)
    {
        $this->usedate = $usedate;
    
        return $this;
    }

    /**
     * Get usedate
     *
     * @return boolean 
     */
    public function getUsedate()
    {
        return $this->usedate;
    }

    /**
     * Set usesubtitle1
     *
     * @param boolean $usesubtitle1
     * @return ContentCategories
     */
    public function setUsesubtitle1($usesubtitle1)
    {
        $this->usesubtitle1 = $usesubtitle1;
    
        return $this;
    }

    /**
     * Get usesubtitle1
     *
     * @return boolean 
     */
    public function getUsesubtitle1()
    {
        return $this->usesubtitle1;
    }

    /**
     * Set subtitle1Label
     *
     * @param string $subtitle1Label
     * @return ContentCategories
     */
    public function setSubtitle1Label($subtitle1Label)
    {
        $this->subtitle1Label = $subtitle1Label;
    
        return $this;
    }

    /**
     * Get subtitle1Label
     *
     * @return string 
     */
    public function getSubtitle1Label()
    {
        return $this->subtitle1Label;
    }

    /**
     * Set usesubtitle2
     *
     * @param boolean $usesubtitle2
     * @return ContentCategories
     */
    public function setUsesubtitle2($usesubtitle2)
    {
        $this->usesubtitle2 = $usesubtitle2;
    
        return $this;
    }

    /**
     * Get usesubtitle2
     *
     * @return boolean 
     */
    public function getUsesubtitle2()
    {
        return $this->usesubtitle2;
    }

    /**
     * Set subtitle2Label
     *
     * @param string $subtitle2Label
     * @return ContentCategories
     */
    public function setSubtitle2Label($subtitle2Label)
    {
        $this->subtitle2Label = $subtitle2Label;
    
        return $this;
    }

    /**
     * Get subtitle2Label
     *
     * @return string 
     */
    public function getSubtitle2Label()
    {
        return $this->subtitle2Label;
    }

    /**
     * Set useparent
     *
     * @param boolean $useparent
     * @return ContentCategories
     */
    public function setUseparent($useparent)
    {
        $this->useparent = $useparent;
    
        return $this;
    }

    /**
     * Get useparent
     *
     * @return boolean 
     */
    public function getUseparent()
    {
        return $this->useparent;
    }

    /**
     * Set parentcategory
     *
     * @param integer $parentcategory
     * @return ContentCategories
     */
    public function setParentcategory($parentcategory)
    {
        $this->parentcategory = $parentcategory;
    
        return $this;
    }

    /**
     * Get parentcategory
     *
     * @return integer 
     */
    public function getParentcategory()
    {
        return $this->parentcategory;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ContentCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    
        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get pages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Add categoryLinks
     *
     * @param \Webberig\CMSBundle\Entity\ContentCategoryLink $categoryLinks
     * @return ContentCategory
     */
    public function addCategoryLink(\Webberig\CMSBundle\Entity\ContentCategoryLink $categoryLinks)
    {
        $this->categoryLinks[] = $categoryLinks;
    
        return $this;
    }

    /**
     * Remove categoryLinks
     *
     * @param \Webberig\CMSBundle\Entity\ContentCategoryLink $categoryLinks
     */
    public function removeCategoryLink(\Webberig\CMSBundle\Entity\ContentCategoryLink $categoryLinks)
    {
        $this->categoryLinks->removeElement($categoryLinks);
    }

    /**
     * Get categoryLinks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategoryLinks()
    {
        return $this->categoryLinks;
    }

    /**
     * Set useGallery
     *
     * @param boolean $useGallery
     * @return ContentCategory
     */
    public function setUseGallery($useGallery)
    {
        $this->useGallery = $useGallery;
    
        return $this;
    }

    /**
     * Get useGallery
     *
     * @return boolean 
     */
    public function getUseGallery()
    {
        return $this->useGallery;
    }

    /**
     * Add pages
     *
     * @param \Webberig\CMSBundle\Entity\ContentPage $pages
     * @return ContentCategory
     */
    public function addPage(\Webberig\CMSBundle\Entity\ContentPage $pages)
    {
        $this->pages[] = $pages;
    
        return $this;
    }

    /**
     * Remove pages
     *
     * @param \Webberig\CMSBundle\Entity\ContentPage $pages
     */
    public function removePage(\Webberig\CMSBundle\Entity\ContentPage $pages)
    {
        $this->pages->removeElement($pages);
    }
}
<?php

namespace Webberig\CMSBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Webberig\CMSBundle\Entity;

class WebberigCMSBundle extends Bundle
{
    public function boot()
    {

        try
        {
            $menuService = \Webberig\BackendBundle\Service\Menu::singleton()
                ->add("menu", "webberig_cms_menu", null, "Menubeheer", 4)
                ->add("languages", "webberig_cms_language_list", null, "Talen", 5)
            ;

            $ccService = $this->container->get("cms.contentcategory");
            //$ccService = new \Webberig\CMSBundle\Service\ContentCategory($em);
            $categories = @$ccService->getList();
            if (count($categories) == 0)
            {
                $ccService->createPagesCategory();
                $categories = $ccService->getList();
            }

            foreach ($categories as $cat)
            {
                $menuService->add("content_" . $cat->getSlug(), "webberig_cms_content_category_index", array("category" => $cat->getSlug()), $cat->getName(), 3);
            }

        } catch(\Doctrine\DBAL\DBALException $e)
        {
            // Ignore DBALException
        } catch(\PDOException $e)
        {
            // Ignore DBALException
        } catch (\Exception $e)
        {
            throw $e;
        }
    }
    public function getParent()
    {
        return "WebberigBackendBundle";
    }


}

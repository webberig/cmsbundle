<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mathieu
 * Date: 3/02/13
 * Time: 17:41
 * To change this template use File | Settings | File Templates.
 */
namespace Webberig\CMSBundle\Service;

class Language
{
    protected $em;
    public function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getById($id)
    {
        return $this->em
            ->getRepository("WebberigCMSBundle:Language")
            ->findOneBy(array("id" => $id));
    }
    public function getByAbbreviation($abbr)
    {
        return $this->em
            ->getRepository("WebberigCMSBundle:Language")
            ->findOneBy(array("abbreviation" => $abbr));
    }

    /**
     * @return \Webberig\CMSBundle\Entity\Language
     */
    public function getFirst()
    {
        return $this->em
            ->getRepository("WebberigCMSBundle:Language")
            ->createQueryBuilder("l")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
    public function getList()
    {
        return $this->em
            ->getRepository("WebberigCMSBundle:Language")
            ->findAll();
    }
    public function save($language)
    {
        $em = $this->em;
        $em->persist($language);
        $em->flush();
    }

    public function populate($hash, \Webberig\CMSBundle\Entity\Language $language = null)
    {
        if (!\Webberig\SharedBundle\Utility\ArrayUtil::isArrayType($hash)) {
            throw new \InvalidArgumentException('$data is not an array');
        }

        $language = $language ? : new \Webberig\CMSBundle\Entity\Language();

        foreach ($hash as $key => $value) {
            try {
                switch ($key) {
                    case 'name':
                        $language->setName($value);
                        break;
                    case 'abbreviation':
                        $language->setAbbreviation($value);
                        break;
                    case 'sequence':
                        $language->setSequence($value);
                        break;
                    case 'isocode':
                        $language->setIsocode($value);
                        break;
                }
            } catch (\Exception $e /* TODO make it more specific instead of just Exception */) {
                throw $e;
            }
        }
        if (isset($hash['active']))
        {
            $language->setActive(1);
        } else {
            $language->setActive(0);
        }

        return $language;
    }

}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mathieu
 * Date: 3/02/13
 * Time: 17:41
 * To change this template use File | Settings | File Templates.
 */
namespace Webberig\CMSBundle\Service;

use Webberig\CMSBundle\Entity\Menugroup;

class Menu
{
    /* @var $em \Doctrine\ORM\EntityManager */
    protected $em;
    public function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getGroupById($id)
    {
        return $this->em
            ->getRepository("WebberigCMSBundle:Menugroup")
            ->findOneBy(array("id" => $id));
    }

    public function getFirstGroup()
    {
        $menu = $this->em
            ->getRepository("WebberigCMSBundle:Menugroup")
            ->createQueryBuilder("l")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
        if ($menu == null) {
            $menu = $this->createDefaultGroup();
        }
        return $menu;
    }

    private function createDefaultGroup() {
        $data = array(

        );
        $menu = new Menugroup();
        $menu->setName("Hoofdmenu");
        $menu->setSequence(1);
        $this->save($menu);
        return $menu;
    }

    public function getGroupList()
    {
        return $this->em
            ->getRepository("WebberigCMSBundle:Menugroup")
            ->findAll();
    }

    public function getMenuItems($groupId, $languageId, $parentId = null)
    {
        $params = array("language" => $languageId);
        if ($parentId) {
            $params["parent"] = $parentId;
        } else {
            $params["menugroup"] = $groupId;
            $params["parent"] = null;
        }
        return $this->em
            ->getRepository("WebberigCMSBundle:Menuitem")
            ->findBy($params, array("sequence" => "asc"));

    }

    /**
     * @param $page \Webberig\CMSBundle\Entity\ContentPage
     * @return array
     */
    public function getMenuItemsByPage($page)
    {
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('mi')
            ->from("WebberigCMSBundle:Menuitem", "mi")
            ->where(
                $qb->expr()->orX(
                    $qb->expr()->eq('mi.page', "?1"),
                    $qb->expr()->eq('mi.url', "?2")
                )
            );

        $qb->setParameter(1, $page->getId());
        $qb->setParameter(2, $page->getFullUrl());

        $results = $qb->getQuery()->getArrayResult();
        return $results;
    }

    /**
     * @param $page ContentPage
     * @return array
     */
    public function getActiveMenuIds($page)
    {

        $arrMenus = array();
        $results = $this->getMenuItemsByPage($page);
        foreach ($results as $menuItem)
        {
            $menuItem = $this->getById($menuItem["id"]);

            $arrMenus[] = $menuItem->getId();
            while ($parent = $menuItem->getParent())
            {
                $menuItem = $parent;
                $arrMenus[] = $menuItem->getId();

            }
        }
        return $arrMenus;
    }

    public function getById($id)
    {
        return $this->em
            ->getRepository("WebberigCMSBundle:Menuitem")
            ->findOneBy(array("id" => $id));
    }

    public function save($menuItem)
    {
        $em = $this->em;
        $em->persist($menuItem);
        $em->flush();
    }

    public function delete($menuItem)
    {
        $em = $this->em;
        $em->remove($menuItem);
        $em->flush();
    }

    public function populate($hash, \Webberig\CMSBundle\Entity\Menuitem $menuItem = null)
    {
        if (!\Webberig\SharedBundle\Utility\ArrayUtil::isArrayType($hash)) {
            throw new \InvalidArgumentException('$data is not an array');
        }

        $menuItem = $menuItem ? : new \Webberig\CMSBundle\Entity\Menuitem();

        foreach ($hash as $key => $value) {
            try {
                switch ($key) {
                    case 'name':
                        $menuItem->setName($value);
                        break;
                    case 'url':
                        $menuItem->setUrl($value);
                        break;
                    case 'page':
                        $pageService = new \Webberig\CMSBundle\Service\ContentPage($this->em);
                        $page = $pageService->getById($value);
                        if ($page) {
                            $menuItem->setPage($page);
                            $menuItem->setUrl($page->getFullUrl());
                        } else {
                            $menuItem->setUrl("#");
                        }
                        break;
                }
            } catch (\Exception $e /* TODO make it more specific instead of just Exception */) {
                throw $e;
            }
        }

        return $menuItem;
    }

    public function setSequence($menuGroup, $sequence)
    {
        $index = 0;

        foreach ($sequence as $itemId => $parentId)
        {
            echo $itemId . " - " . $index++ . "|";
            $item = $this->getById($itemId);
            $item->setSequence($index)
                ->setParent($parentId == 'root' ? null : $this->em->getReference('WebberigCMSBundle:Menuitem', $parentId));
            $this->em->persist($item);
            $this->em->flush();
        }

    }

}
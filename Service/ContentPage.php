<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mathieu
 * Date: 3/02/13
 * Time: 17:41
 * To change this template use File | Settings | File Templates.
 */
namespace Webberig\CMSBundle\Service;
use Webberig\CMSBundle\WebberigCMSBundle;

class ContentPage
{
    protected $em;
    protected $pages;
    protected $galleryLocation;

    public function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->pages = $this->em->getRepository("WebberigCMSBundle:ContentPage");
        $this->setGalleryLocation("uploads/gallery/");
    }

    public function deletePage($page) {
        $this->em->remove($page);
        $this->em->flush();


    }
    public function getById($id)
    {
        return $this->pages
            ->findOneBy(array("id" => $id));
    }
    public function setGalleryLocation($folder) {
        $this->galleryLocation = $folder;
    }

    /**
     * @param $slug string
     * @param $language \Webberig\CMSBundle\Entity\Language
     * @return \Webberig\CMSBundle\Entity\ContentPage|null
     */
    public function getBySlug($slug, $language, $category = 1)
    {
        return $this->pages
            ->findOneBy(array(
                "url" => $slug,
                "language" => $language,
                "category" => $category
            ));
    }

    public function getList($languageId = null, $categoryId = null)
    {
        $params = array();
        if ($languageId !== null) {
            $params = array("language" => $languageId);
        }
        $orderBy = array("title" => "ASC");

        if ($categoryId)
        {
            $categoryService = new ContentCategory($this->em);
            $category = $categoryService->getById($categoryId);
            if (!$category)
                throw new \Exception("Couldn't find category");
            if ($category->getUsedate())
                $orderBy = array("itemdate" => "DESC", "title" => "ASC");

            $params["category"] = $categoryId;
        }
        return $this->pages
            ->findBy($params, $orderBy);
    }
    public function createPagesCategory()
    {
        // Create the "pages" category...
    }

    public function save($page)
    {
        $em = $this->em;
        $em->persist($page);
        $em->flush();
    }

    public function populate($hash, \Webberig\CMSBundle\Entity\ContentPage $page = null)
    {
        if (!\Webberig\SharedBundle\Utility\ArrayUtil::isArrayType($hash)) {
            throw new \InvalidArgumentException('$data is not an array');
        }

        $page = $page ? : new \Webberig\CMSBundle\Entity\ContentPage();

        foreach ($hash as $key => $value) {
            try {
                switch ($key) {
                    case 'content':
                        $page->setContent($value);
                        break;
                    case 'description':
                        $page->setDescription($value);
                        break;
                    case 'intro':
                        $page->setIntro($value);
                        break;
                    case 'itemdate':
                        $page->setItemdate($value);
                        break;
                    case 'startdate':
                        $page->setStartdate($value);
                        break;
                    case 'stopdate':
                        $page->setStopdate($value);
                        break;
                    case 'subtitle1':
                        $page->setSubtitle1($value);
                        break;
                    case 'subtitle2':
                        $page->setSubtitle2($value);
                        break;
                    case 'title':
                        $page->setTitle($value);
                        break;
                    case 'url':
                        $page->setUrl($value);
                        break;
                }
                if (isset($hash['active']))
                {
                    $page->setActive(1);
                } else {
                    $page->setActive(0);
                }
            } catch (\Exception $e /* TODO make it more specific instead of just Exception */) {
            }
        }
        return $page;
    }

    /**
     * @param $page \Webberig\CMSBundle\Entity\ContentPage
     */
    public function getGallery($page) {
        $images = array();
        if (!$page->getCategory()->getUseGallery()) {
            // Don't even bother looking
            return $images;
        }
        $dir = $this->galleryLocation . $page->getId() . "/";

        if (file_exists($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if (is_file($dir . $file)) {
                        $images[] = $this->galleryLocation . $page->getId() . "/" . $file;
                    }
                }
                closedir($dh);
            }
        }
        return $images;
    }
}
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mathieu
 * Date: 3/02/13
 * Time: 17:41
 * To change this template use File | Settings | File Templates.
 */
namespace Webberig\CMSBundle\Service;

class ContentCategory
{
    protected $em;
    protected $categories;

    public function __construct(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->categories = $this->em->getRepository("WebberigCMSBundle:ContentCategory");
    }

    public function getById($id)
    {
        return $this->categories
            ->findOneBy(array("id" => $id));
    }
    public function getBySlug($slug)
    {
        return $this->categories
            ->findOneBy(array("slug" => $slug));
    }
    public function getByUrl($slug)
    {
        return $this->categories
            ->findOneBy(array("urlprefix" => $slug));
    }
    public function getFirst()
    {
        return $this->categories
            ->createQueryBuilder("l")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
    public function getList()
    {
        return $this->categories->findBy(array(),array("sequence" => "asc"));
    }

    public function createPagesCategory()
    {
        // Create the "pages" category...
        $category = new \Webberig\CMSBundle\Entity\ContentCategory();
        $category
            ->setUrlprefix("")
            ->setUseurl(true)
            ->setUseparent(false)
            ->setUsemeta(true)
            ->setUseintro(false)
            ->setUsedaterange(false)
            ->setUsedate(false)
            ->setUsecontent(true)
            ->setName("Pagina's")
            ->setParentcategory(0)
            ->setSequence(1)
            ->setSlug("paginas");

        $this->em->persist($category);
        $this->em->flush();
    }
}
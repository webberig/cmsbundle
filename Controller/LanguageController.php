<?php

namespace Webberig\CMSBundle\Controller;

class LanguageController extends BaseController
{
    public function indexAction()
    {
        \Webberig\BackendBundle\Service\Menu::singleton()->setActive("languages");
        $languages = $this->getLanguageService()->getList();
        return $this->render('WebberigCMSBundle:Language:index.html.twig', array("languages" => $languages));
    }

    public function editAction($language)
    {
        $language = $this->getLanguage($language);
        return $this->render('WebberigCMSBundle:Language:edit.html.twig',
            array(
                "language" => $language
            ));

    }
    public function newAction()
    {
        \Webberig\BackendBundle\Service\Menu::singleton()->setActive("languages");
        $language = new \Webberig\CMSBundle\Entity\Language();

        return $this->render('WebberigCMSBundle:Language:edit.html.twig',
            array(
                "language" => $language
            ));

    }

    public function saveAction($language)
    {
        $language = $this->getLanguageService()->getByAbbreviation($language);

        $request = $this->getRequest()->request;
        $post = $request->all();

        $language = $this->getLanguageService()->populate($post, $language);
        $this->getLanguageService()->save($language);

        return $this->redirect($this->generateUrl("webberig_cms_language_list"));
    }

    public function deleteAction($language)
    {
        $language = $this->getLanguage($language);
        $pages = $language->getPages();
        $langService = $this->get('cms.language');
        $langService->delete($language);


        return $this->redirect($this->generateUrl("webberig_cms_language_list"));
    }

}

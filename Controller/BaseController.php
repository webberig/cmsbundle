<?php

namespace Webberig\CMSBundle\Controller;

class BaseController extends \Webberig\BackendBundle\Controller\BaseController
{
    /**
     * @return \Webberig\CMSBundle\Service\Language
     */
    protected function getLanguageService()
    {
        return $this->get("cms.language");
    }

    /**
     * @return \Webberig\CMSBundle\Service\ContentCategory
     */
    protected function getCategoryService()
    {
        return $this->get("cms.contentcategory");
    }

    /**
     * @return \Webberig\CMSBundle\Service\ContentPage
     */
    protected function getPageService()
    {
        return $this->get("cms.contentpage");
    }

    /**
     * @return \Webberig\CMSBundle\Service\Menu
     */
    protected function getMenuService()
    {
        return $this->get("cms.menu");
    }

    /**
     * @return \Webberig\CMSBundle\Entity\ContentCategory
     */
    protected function getCategory($abbr)
    {
        $category = $this->getCategoryService()->getBySlug($abbr);
        if (!$category)
            throw $this->createNotFoundException('Page not found - Unknown category');

        return $category;

    }

    /**
     * @return \Webberig\CMSBundle\Entity\Language
     */
    protected function getLanguage($abbr)
    {
        $l = $this->getLanguageService()->getByAbbreviation($abbr);
        if (!$l)
            throw $this->createNotFoundException('Page not found - Unknown language');

        return $l;

    }


}
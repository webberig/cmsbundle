<?php

namespace Webberig\CMSBundle\Controller;

use Gregwar\Image\Image;
use Gregwar\ImageBundle\Services\ImageHandling;
use Symfony\Component\HttpFoundation\JsonResponse;
use Webberig\CMSBundle\Entity\ContentPage;

class ContentController extends BaseController
{
    public function indexAction()
    {
        $language = $this->getLanguageService()->getFirst();
        $category = $this->getCategoryService()->getFirst();

        return $this->redirect($this->generateUrl("webberig_cms_content_list", array("category" => $category->getSlug(), "language" => $language->getAbbreviation())));
    }

    public function categoryIndexAction($category)
    {
        $cat = $this->getCategory($category);
        $language = $this->getLanguageService()->getFirst();

        return $this->redirect($this->generateUrl("webberig_cms_content_list", array("category" => $cat->getSlug(), "language" => $language->getAbbreviation())));
    }

    public function listAction($category, $language)
    {
        $cat = $this->getCategory($category);
        $lang = $this->getLanguage($language);

        $pages = $this->getPageService()->getList($lang->getId(), $cat->getId());

        return $this->render('WebberigCMSBundle:Content:list.html.twig', array("pages" => $pages, "category" => $cat, "language" => $lang, "languages" => $this->getLanguageService()->getList()));
    }

    public function pageEditAction($category, $page)
    {
        $cat = $this->getCategory($category);

        $p = $this->getPage($page);
        if ($p->getCategory()->getId() != $p->getId())
            $this->createNotFoundException('Page not found - Page category does not match URL category');

        $gallery = $this->getPageService()->getGallery($p);

        return $this->render('WebberigCMSBundle:Content:pageEdit.html.twig',
            array(
                "category" => $cat,
                "page" => $p,
                "language" => $p->getLanguage(),
                "gallery" => $gallery
            ));

    }

    public function newAction($category, $language)
    {
        $cat = $this->getCategory($category);
        $lang = $this->getLanguage($language);

        $page = new \Webberig\CMSBundle\Entity\ContentPage();
        $page->setActive(true);

        return $this->render('WebberigCMSBundle:Content:pageEdit.html.twig',
            array(
                "category" => $cat,
                "language" => $lang,
                "page" => $page
            ));

    }

    public function pageSaveAction($category, $page)
    {
        /** @var $pageService \Webberig\CMSBundle\Service\ContentPage */
        $pageService = $this->get('cms.contentpage');

        $p = $this->getPage($page);
        $cat = $this->getCategory($category);

        $request = $this->getRequest()->request;
        $post = $request->all();


        $p = $pageService->populate($post, $p);
        $pageService->save($p);

        $this->getPage($page);

        $this->addFlashSuccess("De pagina is bewaard.");
        return $this->redirect($this->generateUrl("webberig_cms_content_list", array("category" => $category, "language" => $p->getLanguage()->getAbbreviation())));
    }

    public function newPageSaveAction($category, $language)
    {
         /*
         * @var $pageService \Webberig\CMSBundle\Service\ContentPage
         * @var $langService \Webberig\CMSBundle\Service\Language
         */
        $pageService = $this->get('cms.contentpage');
        $langService = $this->get('cms.language');

        // Request
        $request = $this->getRequest()->request;
        $post = $request->all();

        $cat = $this->getCategory($category);
        $lang = $this->getLanguage($language);


        $page = new \Webberig\CMSBundle\Entity\ContentPage();
        $page = $pageService->populate($post, $page);
        $page
            ->setLanguage($lang)
            ->setCategory($cat);
        $pageService->save($page);
        $this->addFlashSuccess("De pagina is toegevoegd.");


        return $this->redirect($this->generateUrl("webberig_cms_content_list", array("category" => $category, "language" => $lang->getAbbreviation())));
    }

    public function deletePhotoAction ($category, $language, $page, $img) {
        $category = $this->getCategory($category);
        $language = $this->getLanguage($language);
        $page = $this->getPage($page);

        $folder = "uploads/gallery/" . $page->getId() . "/";

        if (!is_dir($folder)) {
            mkdir($folder);
        }
        $filename = $folder . $img;
        unlink($filename);

        return new JsonResponse(array(
            "success" => true
        ));
    }

    public function uploadPhotoAction ($category, $language, $page)
    {
        $category = $this->getCategory($category);
        $language = $this->getLanguage($language);
        $page = $this->getPage($page);

        /**
         * @var $file \Symfony\Component\HttpFoundation\File\UploadedFile
         */
        $filebag = $this->getRequest()->files->all();
        $file = $filebag["files"][0];
        if (!is_object($file)) {
            return new JsonResponse(array(
                "success" => false,
                "message" => "Geen files gevonden"
            ));
        }

        $image = new Image($file->getRealPath());

        try {
            if ($image->width() > 1200 || $image->height() > 1200) {
                $image = $image->cropResize(1200,1200);
            }

        } catch(\Exception $e) {
            return new JsonResponse(array(
                "success" => false,
                "message" => "Opgeladen bestand is geen geldige afbeelding: " . $e->getMessage()
            ));
        }


        //$folder = $this->getRequest()->server->get('DOCUMENT_ROOT');
        $folder = "uploads/gallery/" . $page->getId() . "/";

        if (!is_dir($folder)) {
            mkdir($folder);
        }
        $filename = $folder . $file->getClientOriginalName();

        if (is_file($filename)) {
            return new JsonResponse(array(
                "success" => false,
                "message" => "Bestand bestaat reeds"
            ));
        }
        $image->save($filename);

        /**
         * @var $imageHandler ImageHandling
         */
        $image->setCacheDir("cache");
        $url = $image->zoomCrop(200,200)->jpeg();

        return new JsonResponse(array(
            "success" => true,
            "url" => $url
        ));

    }

    protected function getCategory($id)
    {
        $cat = parent::getCategory($id);
        \Webberig\BackendBundle\Service\Menu::singleton()->setActive("content_" . $id);
        return $cat;
    }

    /**
     * @param $id
     * @return ContentPage
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function getPage($id)
    {
        $page = $this->getPageService()->getById($id);
        if ($page==null)
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("Pagina niet gevonden");

        return $page;
    }

    public function deleteAction($category, $language, $page) {
        $page = $this->getPageService()->getById($page);
        $this->getPageService()->deletePage($page);
        $lang = $this->getLanguage($language);
        $this->addFlashSuccess("De pagina is verwijderd.");


        return $this->redirect($this->generateUrl("webberig_cms_content_list", array("category" => $category, "language" => $lang->getAbbreviation())));

    }
}

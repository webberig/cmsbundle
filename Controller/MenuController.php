<?php

namespace Webberig\CMSBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class MenuController extends BaseController
{
    public function indexAction()
    {
        \Webberig\BackendBundle\Service\Menu::singleton()->setActive("menu");

        $group = $this->getMenuService()->getFirstGroup();
        $language = $this->getLanguageService()->getFirst();

        return $this->redirect(
            $this->generateUrl("webberig_cms_menu_list",
                array(
                    "menuId" => $group->getId(),
                    "languageId" => $language->getAbbreviation()
                )
            )
        );
    }

    public function menuAction($menuId, $languageId)
    {
        \Webberig\BackendBundle\Service\Menu::singleton()->setActive("menu");

        $currentgroup = $this->getMenugroup($menuId);
        $language = $this->getLanguage($languageId);
        $languages = $this->getLanguageService()->getList();

        $menus = $this->getMenuService()->getMenuItems($menuId, $language->getId());
        $menugroups = $this->getMenuService()->getGroupList();
        $pages = $this->getPageService()->getList();
        $pagesArray = array();
        foreach($pages as $p) {
            $pagesArray[$p->getId()] = $p->getTitle();
        }

        return $this->render('WebberigCMSBundle:Menu:menu.html.twig',
            array(
                "menugroup" => $currentgroup,
                "menugroups" => $menugroups,
                "language" => $language,
                "languages" => $languages,
                "menus" => $menus,
                "pages" => $pagesArray
            ));

    }

    public function newSaveAction($menuId, $languageId)
    {
        \Webberig\BackendBundle\Service\Menu::singleton()->setActive("menu");
        $currentgroup = $this->getMenugroup($menuId);
        $language = $this->getLanguage($languageId);

        // Populate the item
        $menuItem = new \Webberig\CMSBundle\Entity\Menuitem();

        /* @var $menuService \Webberig\CMSBundle\Service\Menu */
        $menuService = $this->get('cms.menu');

        $request = $this->getRequest()->request;
        $post = $request->all();
        $menuItem = $menuService->populate($post, $menuItem);

        $menuItem
            ->setLanguage($language)
            ->setMenugroup($currentgroup);

        $menuService->save($menuItem);
        return $this->redirect($this->generateUrl("webberig_cms_menu_list", array(
            "menuId" => $currentgroup->getId(),
            "languageId" => $language->getAbbreviation()
        )));
    }


    public function editSaveAction($menuId, $languageId, $itemId)
    {
        $currentgroup = $this->getMenugroup($menuId);
        $language = $this->getLanguage($languageId);
        $menuItem = $this->getMenuService()->getById($itemId);


        // Populate the item
        $request = $this->getRequest()->request;
        $post = $request->all();
        $menuItem = $this->getMenuService()->populate($post, $menuItem);

        $this->getMenuService()->save($menuItem);
        return $this->redirect($this->generateUrl("webberig_cms_menu_list", array(
            "menuId" => $currentgroup->getId(),
            "languageId" => $language->getAbbreviation()
        )));
    }

    public function deleteAction($menuId, $languageId, $itemId)
    {
        $language = $this->getLanguage($languageId);
        $currentgroup = $this->getMenugroup($menuId);
        $menuItem = $this->getMenuService()->getById($itemId);

        $this->getMenuService()->delete($menuItem);

        return $this->redirect($this->generateUrl("webberig_cms_menu_list", array(
            "menuId" => $currentgroup->getId(),
            "languageId" => $language->getAbbreviation()
        )));
    }

    public function setSequenceAction($menuId)
    {
        $menuService = $this->get('cms.menu');
        $menuGroup = $this->getMenugroup($menuId);

        $request = $this->getRequest()->request;
        $post = $request->all();
        $menuService->setSequence($menuGroup, $post['menuItem']);

        return new JsonResponse(array());
    }

    private function getMenugroup($menuId)
    {
        /* @var $menuService \Webberig\CMSBundle\Service\Menu */
        $menuService = $this->get('cms.menu');
        $menuGroup = $menuService->getGroupById($menuId);
        if ($menuGroup==null)
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException("Menugroep niet gevonden");

        \Webberig\BackendBundle\Service\Menu::singleton()->setActive("menu");
        return $menuGroup;
    }

}

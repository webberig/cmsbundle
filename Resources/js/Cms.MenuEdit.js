var Cms = Cms || {};
Cms.MenuEdit = {
    config: {
        sequenceUrl: null
    },
    init: function() {
        var that = Cms.MenuEdit;
        that.form = $("form");
        that.form.hide();
        that.menuItems = $(".selectable");
        that.menuItems.click(that.menuSelect);
        that.sortableList = $('ol.sortable');

        that.sortableList.nestedSortable
        ({
            disableNesting: 'no-nest',
            forcePlaceholderSize: true,
            handle: 'div',
            helper:	'clone',
            items: 'li',
            maxLevels: 3,
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            stop: function()
            {
                var that = Cms.MenuEdit,
                    menuKeys = that.sortableList.nestedSortable('serialize');
                $.post(Cms.MenuEdit.config.sequenceUrl, menuKeys);
            }
        });
        $("#btnAddNew").click(that.menuSelect);

    },
    menuSelect: function(e) {
        var that = Cms.MenuEdit,
            $this = $(this);
        that.menuItems.removeClass("selected");
        $this.addClass("selected");
        that.showForm.call(this);
    },
    showForm: function() {
        var that = Cms.MenuEdit,
            $this = $(this),
            form;
        form = $("form");
        if (typeof $this.data("menu-label") !== 'undefined') {
            form.find("legend").text($this.data("menu-label"));
        }
        if (typeof $this.data("menu-action") !== 'undefined') {
            form.attr("action", $this.data("menu-action"));
        }
        if (typeof $this.data("menu-delete") !== 'undefined') {
            form.find("#frmMenuItem-Delete")
                .attr("href", $this.data("menu-delete"))
                .show();
        } else {
            form.find("#frmMenuItem-Delete")
                .hide();
        }
        if (typeof $this.data("menu-page") !== 'undefined') {
            form.find("#frmMenuItem_page")
                .val($this.data("menu-page"));
        } else {
            form.find("#frmMenuItem_page")
                .val(null);
        }
        if (typeof $this.data("menu-name") !== 'undefined') {
            form.find("#frmMenuItem_name")
                .val($this.data("menu-name"));
        } else {
            form.find("#frmMenuItem_name")
                .val("");
        }

        form.fadeIn();
    }
}
var Cms = Cms || {};
Cms.PageGalleryEdit = {
    instances: [],
    init: function() {
        var galleries = $(".cms-gallery");
        $(".cms-gallery").each(function() {
            Cms.PageGalleryEdit.instances.push(new Cms.PageGalleryEdit.object($(this)));
        });

        $(document).bind('drop dragover', function (e) {
            e.preventDefault();
        });
    },
    object: function(container) {
        var self = this;
        this.container = container;
        this.input = this.container.find(".upload");

        this.input.fileupload({
            dropZone: this.container.find(".dropzone"),
            done: function (e, data) {
                var div = self.container.find(".item.loading").first(),
                    response = data._response.result;
                if (response.success == true) {
                    div.removeClass("loading");
                    var img = $("<img />");
                    img
                        .hide()
                        .attr("src", "/" + response.url)
                        .on("load", function() {
                            $(this).fadeIn('slow');
                            div.spin(false);
                        })
                        .appendTo(div);

                } else {
                    div.remove();
                    alert(response.message);
                }
            },
            add: function (e, data) {
                var div = $('<div class="item loading"></div>');
                self.container.find(".items").append(div);
                div.spin();
                data.submit();
            },
            fail: function(e, data) {
                var div = self.container.find(".item.loading").first();
                div.fadeOut(function() {
                    $(this).remove();
                })
                alert("Er heeft zich een onbekende fout voorgedaan: " + data.errorThrown);
            }
        });
    }
};
Cms.PageGalleryEdit.init();